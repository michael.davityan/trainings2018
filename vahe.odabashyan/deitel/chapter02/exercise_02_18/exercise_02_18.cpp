/// Program that comapres user provided numbers

#include <iostream> /// allows the program to input and output data

/// function main begins program execution

int
main()
{
    std::cout << "Please enter two numbers: ";
    int number1, number2; /// two variables where user provided values will be stored
    std::cin >> number1 >> number2;

    if (number1 > number2) {
        std::cout << number1 << " is larger" << std::endl;
	return 0;
    }

    if (number2 > number1) {
        std::cout << number2 << " is larger" << std::endl;
	return 0;
    }

    std::cout << "These numbers are equal" << std::endl;

    return 0; /// indicates the successful completion of the program
} /// end function main

