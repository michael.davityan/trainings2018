/// A program that displays a checkerboard pattern.

#include <iostream> /// allows the program to input and output data

/// function main begins program execution

int
main()
{
    std::cout << "* * * * * * * *\n";
    std::cout << " * * * * * * * *\n";
    std::cout << "* * * * * * * *\n";
    std::cout << " * * * * * * * *\n";
    std::cout << "* * * * * * * *\n";
    std::cout << " * * * * * * * *\n";
    std::cout << "* * * * * * * *\n";
    std::cout << " * * * * * * * *\n\n";

    std::cout << "* * * * * * * *\n * * * * * * * *\n* * * * * * * *\n * * * * * * * *\n" << "* * * * * * * *\n * * * * * * * *\n* * * * * * * *\n * * * * * * * *\n";

    return 0; /// program completed successfully
} /// end function main

