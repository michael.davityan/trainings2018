#include <iostream>

class Layer
{
public:
    int getLayerNumber() { return layerNumber_; }
    void setLayerNumber(int number) { layerNumber_ = number; }

    std::string getLayerName() { return layerName_; }
    void setLayerName(std::string name) { layerName_ = name; }

    void show()
    {
        std::cout << "Layer " << layerNumber_ << ": " << layerName_ << std::endl;
    }

private:
    int layerNumber_;
    std::string layerName_;
};

int
main()
{
    int layerNumber = 1;
    std::string layerName = "M1";

    Layer layer;

    std::cout << "Initial layer number: " << layer.getLayerNumber() << std::endl;
    std::cout << "Initial layer name: " << layer.getLayerName() << std::endl;

    layer.setLayerNumber(layerNumber);
    layer.setLayerName(layerName);
    layer.show(layerNumber, layerName);

    return 0;
}

