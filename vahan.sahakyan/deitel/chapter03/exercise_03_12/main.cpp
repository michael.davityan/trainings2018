#include "Account.hpp"
#include <iostream>

int
main()
{
    Account account1(45000);
    Account account2(2200);

    int credit1, debit1;
    std::cout << "-------ACCOUNT 1-------\n";
    std::cout << "Enter credit amount: ";
    std::cin >> credit1;
    account1.credit(credit1);
    std::cout << "Enter debit amount: ";
    std::cin >> debit1;
    account1.debit(debit1);

    std::cout << "CURRENT BALANCE: " << account1.getBalance() << "\n\n" << std::endl;

    int credit2, debit2;
    std::cout << "-------ACCOUNT 2-------\n";
    std::cout << "Enter credit amount: ";
    std::cin >> credit2;
    account2.credit(credit2);
    std::cout << "Enter debit amount: ";
    std::cin >> debit2;
    account2.debit(debit2);

    std::cout << "CURRENT BALANCE: " << account2.getBalance() << std::endl;

    std::cout << "\n\nThank you for choosing our bank!" << std::endl;
    return 0;
}

