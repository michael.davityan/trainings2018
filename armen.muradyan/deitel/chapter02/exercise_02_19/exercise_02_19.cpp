#include <iostream>

int
main()
{
    int number1, number2, number3;
    std::cout << "Please enter first number: ";
    std::cin >> number1;

    std::cout <<"Please enter second number: ";
    std::cin >> number2; 

    std::cout<<"Please enter third number:";
    std::cin >> number3;

    int max = number1;
    if (number2 > max) {
        max = number2;
    }
    if (number3 > max) {
        max = number3;
    }
    int min = number1;
    if (number2 < min) {
        min = number2;
    }
    if (number3 < min) {
        min = number3;
    }

    std::cout << "Sum is " << (number1 + number2 + number3) << std::endl;
    std::cout << "Average is " << (number1 + number2 + number3) / 3 << std::endl;
    std::cout << "Product is " << (number1 * number2 * number3) << std::endl;  
    std::cout << "Smalest is " << min << std::endl;
    std::cout << "Greatest is " << max << std::endl;
                                                                         
    return 0;
}
