y = ax^3+7 = a * x * x * x + 7


a. y = a * x * x * x + 7 ; (true)
b. y = a * x * x * ( x + 7 );(false)
c. y = ( a * x ) * x * ( x + 7 );(false)
d. y = (a * x) * x * x + 7 ;(true)
e. y = a * ( x * x * x ) + 7 ;(true)
f. y = a * x * ( x * x + 7 );(false)
